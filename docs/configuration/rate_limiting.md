---
redirect_to: 'proxy.md#handling-rate-limited-requests'
remove_date: '2020-09-25'
---

This document was moved to [another location](proxy.md#handling-rate-limited-requests).
